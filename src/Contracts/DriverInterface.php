<?php

namespace Yusefmobasheri\Filemanager\Contracts;

interface DriverInterface
{
    /**
     * List contents of a directory.
     *
     * @param string|null $directory
     *
     * @return array
     */
    public function list(string $directory = null): array;

    /**
     * Add new file to destination path.
     *
     * @param string|resource $content
     * @param string $path
     * @return mixed
     */
    public function addFile(string $content, string $path);

    /**
     * Delete specific file.
     *
     * @param string $filename
     * @return bool
     */
    public function delete(string $filename): bool;

    /**
     * Returns specific file meta.
     *
     * @param string $filename
     * @return array
     */
    public function getMetadata(string $filename): array;

    /**
     * Returns specific file content.
     *
     * @param string $filename
     * @return string
     */
    public function getContent(string $filename): string;
}