<?php

namespace Yusefmobasheri\Filemanager\Drivers;

use SplFileInfo;
use LogicException;
use DirectoryIterator;
use GuzzleHttp\Psr7\MimeType;
use Yusefmobasheri\Filemanager\Contracts\DriverInterface;
use Yusefmobasheri\Filemanager\Exception\LocalDriverRootNotDefinedException;

class LocalStorageDriver implements DriverInterface
{
    /**
     * @var mixed|null
     */
    private $root;

    /**
     * LocalStorageDriver constructor.
     *
     * @param array $config
     * @throws LocalDriverRootNotDefinedException
     */
    public function __construct(array $config)
    {
        $this->root = isset($config['root']) && !empty($config['root']) ? $config['root'] : null;

        if (is_null($this->root)) {
            throw new LocalDriverRootNotDefinedException('Local driver\'s root path not defined in config');
        }

        if (!is_dir($this->root) || !is_readable($this->root)) {
            throw new LogicException('The root path ' . $this->root . ' is not readable.');
        }
    }

    /**
     * @inheritDoc
     */
    public function list(string $directory = null): array
    {
        $result = [];
        if (!is_null($directory)) {
            $directory = rtrim($this->root, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . ltrim(
                    $directory,
                    DIRECTORY_SEPARATOR
                );
        } else {
            $directory = $this->root;
        }

        if (!is_dir($directory)) {
            return [];
        }

        $iterator = new DirectoryIterator($directory);

        foreach ($iterator as $file) {
            $path = $this->getFilePath($file);

            if (preg_match('#(^|/|\\\\)\.{1,2}$#', $path)) {
                continue;
            }

            $result[] = $this->mapFileInfo($file);
        }

        unset($iterator);

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function addFile(string $content, string $path): string
    {
        $directory     = dirname($path);
        $normalizedDir = $this->normalizePath($directory);
        if (!is_dir($normalizedDir)) {
            mkdir($normalizedDir, 0777, true);
        }
        $newPath = $this->normalizePath($path);
        file_put_contents($newPath, $content);

        return $newPath;
    }

    /**
     * @inheritDoc
     */
    public function delete(string $filename): bool
    {
        $path = $this->normalizePath($filename);

        return @unlink($path);
    }

    /**
     * @inheritdoc
     */
    public function getMetadata(string $filename): array
    {
        $path = $this->normalizePath($filename);
        $info = new SplFileInfo($path);

        return $this->mapFileInfo($info);
    }

    /**
     * @inheritDoc
     */
    public function getContent(string $filename): string
    {
        $path = $this->normalizePath($filename);
        if (!is_file($path)) {
            throw new \InvalidArgumentException('Filename argument is not path of file.');
        }

        return file_get_contents($path);
    }

    /**
     * Get the normalized path from a SplFileInfo object.
     *
     * @param SplFileInfo $file
     *
     * @return string
     */
    protected function getFilePath(SplFileInfo $file): string
    {
        $path = $file->getPathname();

        return trim(str_replace('\\', DIRECTORY_SEPARATOR, $path), DIRECTORY_SEPARATOR);
    }

    /**
     * @param SplFileInfo $file
     *
     * @return array
     */
    protected function mapFileInfo(SplFileInfo $file): array
    {
        $path       = $this->getFilePath($file);
        $normalized = [
            'type' => $file->getType(),
            'path' => $path,
        ];

        $normalized['timestamp'] = $file->getMTime();

        if ($normalized['type'] === 'file') {
            $normalized['size'] = $file->getSize();
        }

        if ($file->getType() != 'dir') {
            $normalized['extension'] = $file->getExtension();
            $normalized['mimetype']  = $this->getMimetype($path);
        }

        return $normalized;
    }

    /**
     * Returns mime type of file
     *
     * @param $path
     * @return string|null
     */
    protected function getMimetype($path): ?string
    {
        return MimeType::fromFilename($path);
    }

    /**
     * Concat root path and filename
     *
     * @param string $path
     * @return string
     */
    protected function normalizePath(string $path): string
    {
        return rtrim($this->root, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . ltrim($path, DIRECTORY_SEPARATOR);
    }
}