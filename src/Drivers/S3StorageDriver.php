<?php

namespace Yusefmobasheri\Filemanager\Drivers;

use Aws\S3\S3Client;
use Aws\Credentials\Credentials;
use Yusefmobasheri\Filemanager\Contracts\DriverInterface;

/**
 * Class S3StorageDriver
 * @package Yusefmobasheri\Filemanager\Drivers
 */
class S3StorageDriver implements DriverInterface
{
    /**
     * @var S3Client
     */
    private $s3Client;
    /**
     * @var string
     */
    private $bucket;

    /**
     * S3StorageDriver constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $key      = $config['key'] ?? null;
        $secret   = $config['secret'] ?? null;
        $bucket   = $config['bucket'] ?? null;
        $url      = $config['url'] ?? null;
        $endpoint = $config['endpoint'] ?? null;
        if (empty($key) || empty($secret) || empty($bucket) || empty($url) || empty($endpoint)) {
            throw new \InvalidArgumentException('The config of s3 driver is invalid.');
        }

        $this->bucket   = $bucket;
        $credentials    = new Credentials($key, $secret);
        $this->s3Client = new S3Client(
            [
                'url'         => $url,
                'credentials' => $credentials,
                'endpoint'    => $endpoint,
                'version'     => 'latest',
                'region'      => 'us-west-2',
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function list(string $directory = null): array
    {
        $args = ['Bucket' => $this->bucket];
        if (!is_null($directory)) {
            $args['Prefix'] = $directory;
        }

        return $this->s3Client->listObjects($args)->get('Contents');
    }

    /**
     * @inheritDoc
     */
    public function addFile(string $content, string $path)
    {
        return $this->s3Client->putObject(
            [
                'Bucket' => $this->bucket,
                'ACL'    => 'public-read',
                'Body'   => $content,
                'Key'    => $path
            ]
        )->get('ObjectURL');
    }

    /**
     * @inheritDoc
     */
    public function delete(string $filename): bool
    {
        $this->s3Client->deleteObject(
            [
                'Bucket' => $this->bucket,
                'Key'    => $filename
            ]
        );

        return true;
    }

    /**
     * @inheritDoc
     */
    public function getMetadata(string $filename): array
    {
        return $this->s3Client->getObject(
            [
                'Bucket' => $this->bucket,
                'Key'    => $filename
            ]
        )->get('Metadata');
    }

    /**
     * @inheritDoc
     */
    public function getContent(string $filename): string
    {
        return $this->s3Client->getObject(
            [
                'Bucket' => $this->bucket,
                'Key'    => $filename
            ]
        )->get('Body');
    }
}