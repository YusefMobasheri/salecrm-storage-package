<?php

namespace Yusefmobasheri\Filemanager;

use Yusefmobasheri\Filemanager\Contracts\DriverInterface;
use Yusefmobasheri\Filemanager\Exception\DriverNotFoundException;
use Yusefmobasheri\Filemanager\Exception\DriverNotSupportedException;
use Yusefmobasheri\Filemanager\Exception\DriverConfigNotFoundException;

class StorageManager
{
    /**
     * @var array
     */
    private $drivers = [];

    /**
     * Get a driver instance.
     *
     * @param string|null $driver
     * @return DriverInterface
     * @throws DriverConfigNotFoundException
     * @throws DriverNotFoundException
     * @throws DriverNotSupportedException
     */
    public function driver(string $driver = null): DriverInterface
    {
        if (is_null($driver)) {
            $driver = config('default');
        }

        if (!key_exists($driver, config('drivers'))) {
            throw new DriverNotFoundException('could not find driver');
        }

        return $this->drivers[$driver] ?? $this->get($driver);
    }

    /**
     * Get a driver instance.
     *
     * @param $driver
     * @return DriverInterface
     * @throws DriverConfigNotFoundException
     */
    private function get($driver): DriverInterface
    {
        $config = config('driver_configs')[$driver] ?? null;

        if (is_null($config)) {
            throw new DriverConfigNotFoundException("Driver {$driver} does not have configuration.");
        }

        $driverClass = config('drivers')[$driver];

        return new $driverClass($config);
    }
}