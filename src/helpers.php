<?php

if (!function_exists('config')) {
    /**
     * Returns package configs
     *
     * @param string|null $key
     * @param null $default
     * @return mixed|null
     */
    function config(string $key = null, $default = null)
    {
        $config = include __DIR__ . '/../config/config.php';
        if (is_null($key)) {
            return $config;
        }
        return $config[$key] ?? $default;
    }
}