<?php

use Yusefmobasheri\Filemanager\StorageManager;

require_once 'vendor/autoload.php';

$storageManager = new StorageManager();
$driver         = $storageManager->driver(); // set driver 'local' or 's3'

$content  = "Hello world";
$filepath = 'test.txt';
$driver->addFile($content, $filepath); // add a new file with specific path and content

$metaData = $driver->getMetadata($filepath); // returns file's meta data

$content = $driver->getContent($filepath); // returns file content

$list = $driver->list(); // returns files and folders list of specific path

$driver->delete($filepath); // returns true or false