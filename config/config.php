<?php

use Yusefmobasheri\Filemanager\Drivers\S3StorageDriver;
use Yusefmobasheri\Filemanager\Drivers\LocalStorageDriver;

return [
    // set default driver
    'default' => 'local',

    // list of supported drivers
    'drivers' => [
        'local' => LocalStorageDriver::class,
        's3'    => S3StorageDriver::class,
    ],

    // here you may configure drivers
    'driver_configs'   => [
        'local' => [
            // define root of local storage folder for management
            'root'   => __DIR__.'/../',
        ],

        's3' => [
            'key'      => '',
            'secret'   => '',
            'bucket'   => '',
            'url'      => '',
            'endpoint' => '',
        ],
    ],
];